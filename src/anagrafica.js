
$(document).ready(function() {
    $('#example').DataTable( {
        "ajax":({
           url: " http://localhost:3000/arrayjson",
           type: "GET",
           dataSrc: "",
           dataType: "json"
        }),
        "columns": [
            { "data": "idanagrafica" },
            { "data": "RagioneSociale" },
            { "data": "Indirizzo" },
            { "data": "Citta" },
            { "data": "Prov" },
            { "data": "CAP" },
            { "data": "PIVA" },
            { "data": "CF" },
            { "data": "Telefono" },
            { "data": "Fax" },
            { "data": "Email" }
            
        ]

    } );
} );
